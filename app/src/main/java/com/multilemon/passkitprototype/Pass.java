package com.multilemon.passkitprototype;

import android.graphics.Color;

import java.util.Date;
import java.util.List;

/**
 * Created by MultiLemon on 2016/10/30.
 */

public class Pass {

    private int id;
    private String description;

    public Pass(int id, String description) {
        this.id = id;
        this.description = description;
    }

    private int formatVersion;
    private String organizationName;
    private String passTypeIdentifier;
    private String serialNumber;
    private String teamIdentifier;
    private Date expirationDate;
    private boolean voided;

//    private EventTicket eventTicket;

    private Color backgroundColor;
    private Color foregroundColor;
    private Color labelColor;
    private boolean suppressStripShine;
    private String authenticationToken;
    private String webServiceURL;

//    private class EventTicket {
//
//        private List<AuxiliaryFields> auxiliaryFields;
//        private List<BackFields> backFields;
//        private List<SecondaryFields> secondaryFields;
//
//        private class AuxiliaryFields {
//            private String key;
//            private String label;
//            private String textAlignment;
//            private String value;
//        }
//
//        private class BackFields {
//            private String key;
//            private String label;
//            private String changeMessage;
//            private String value;
//            private String attributedValue;
//        }
//
//        private class SecondaryFields {
//            private String key;
//            private String label;
//            private String textAlignment;
//            private String value;
//        }
//
//    }


    public String getDescription() {
        return description;
    }

    public int getFormatVersion() {
        return formatVersion;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public String getPassTypeIdentifier() {
        return passTypeIdentifier;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getTeamIdentifier() {
        return teamIdentifier;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public boolean isVoided() {
        return voided;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public Color getForegroundColor() {
        return foregroundColor;
    }

    public Color getLabelColor() {
        return labelColor;
    }

    public boolean isSuppressStripShine() {
        return suppressStripShine;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public String getWebServiceURL() {
        return webServiceURL;
    }
}
