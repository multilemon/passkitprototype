package com.multilemon.passkitprototype;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PassDetailActivity extends Activity implements PassAdapter.PassAdapterListener {

    @BindView(R.id.passRecyclerView)
    RecyclerView mPassRecyclerView;
    @BindView(R.id.expandedLayout)
    LinearLayout expandedLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_detail);
        ButterKnife.bind(this);
        expandedLayout.setVisibility(View.VISIBLE);

        setupProtoTypeMockUp();
    }

    private void setupProtoTypeMockUp() {
        PassAdapter adapter = new PassAdapter(generateMockUpPass(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mPassRecyclerView.setAdapter(adapter);
        mPassRecyclerView.setLayoutManager(linearLayoutManager);
//        mPassRecyclerView.setLayoutManager(new OverlapLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mPassRecyclerView.setHasFixedSize(true);
        mPassRecyclerView.addItemDecoration(new PassStackItemDecoration(
                getResources().getDimensionPixelSize(R.dimen.stackDetailOffset),
                getResources().getDimensionPixelSize(R.dimen.stackOffsetBottom)));

        mPassRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private List<Pass> generateMockUpPass() {
        List<Pass> passList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            passList.add(new Pass(i, "This is pass " + i));
        }

        return passList;
    }

    @Override
    public void onPassClicked(View itemView, Pass pass) {

    }
}
