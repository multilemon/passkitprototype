package com.multilemon.passkitprototype;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by MultiLemon on 2016/10/30.
 */

public class PassStackItemDecoration extends RecyclerView.ItemDecoration {

    private int stackOverlap = 0;
    private int stackOverlapBottom = 0;

    public PassStackItemDecoration(int stackOverlap, int stackOverlapBottom) {
        this.stackOverlap = stackOverlap;
        this.stackOverlapBottom = stackOverlapBottom;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//        super.getItemOffsets(outRect, view, parent, state);
        int position = parent.getChildAdapterPosition(view);

        if (position == RecyclerView.NO_POSITION) {
            return;
        }

        if (position != 0) {
            outRect.set(0, stackOverlap, 0, 0);
        }
//        outRect.set(0, stackOverlap, 0, 0);

    }
}
