package com.multilemon.passkitprototype;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by MultiLemon on 2016/10/30.
 */

public class OverlapLayoutManager extends LinearLayoutManager {

    private int mLastVisiblePosition;

    public OverlapLayoutManager(Context context) {
        super(context);
    }

    public OverlapLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(
                RecyclerView.LayoutParams.WRAP_CONTENT,
                RecyclerView.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        super.onLayoutChildren(recycler, state);

        detachAndScrapAttachedViews(recycler);


        // 1. get view by position
        View view = recycler.getViewForPosition(mLastVisiblePosition);

        // 2. add view to the recycler view
        addView(view);

        // 3. get view location on the screen

        // 4. layout this view

        // 5. increment view position
        mLastVisiblePosition++;

    }


    @Override
    public boolean canScrollVertically() {
        return true;
    }

    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
        // any fancy stuff when scrolling goes here

        return super.scrollVerticallyBy(dy, recycler, state);
    }
}
