package com.multilemon.passkitprototype;

import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MultiLemon on 2016/10/27.
 */

class PassAdapter extends RecyclerView.Adapter<PassAdapter.PassViewHolder> {

    private static final int TYPE_MINI = 0;
    private static final int TYPE_FULL = 1;

    private List<Pass> mPassList;
    private PassAdapterListener mListener;

    PassAdapter(List<Pass> passList, PassAdapterListener listener) {
        mPassList = passList;
        mListener = listener;
    }

    @Override
    public PassViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_MINI) {
            return new PassViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pass, parent, false));
        } else if (viewType == TYPE_FULL) {
            return new PassViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pass, parent, false));
        }

        throw new TypeNotPresentException("This view type " + viewType + " is not supported", new Throwable("This view type " + viewType + " is not supported"));
    }

    @Override
    public void onBindViewHolder(PassViewHolder holder, int position) {
        Pass pass = mPassList.get(position);
//        if (pass != null) {
//            holder.passText.setText(pass.getDescription());
//
//        }
    }

    @Override
    public int getItemCount() {
        return mPassList == null ? 0 : mPassList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return getItemCount() == position + 1 ? TYPE_FULL : TYPE_MINI;
    }

    class PassViewHolder extends RecyclerView.ViewHolder {

//        @BindView(R.id.passLayout)
//        RelativeLayout passLayout;
        @BindView(R.id.expandedLayout)
        LinearLayout expandedLayout;

        PassViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setBackgroundColor(Color.TRANSPARENT);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    expandedLayout.setVisibility(View.VISIBLE);
                    if (mListener != null) {
                        mListener.onPassClicked(itemView, mPassList.get(getAdapterPosition()));
                    }
                }
            });
        }
    }

    public interface PassAdapterListener {
        void onPassClicked(View itemView, Pass pass);
    }
}
