package com.multilemon.passkitprototype;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements PassAdapter.PassAdapterListener {

    @BindView(R.id.activity_main)
    RelativeLayout mActivityMain;
    @BindView(R.id.passRecyclerView)
    RecyclerView mPassRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupProtoTypeMockUp();
    }

    private void setupProtoTypeMockUp() {
        PassAdapter adapter = new PassAdapter(generateMockUpPass(), this);
        mPassRecyclerView.setAdapter(adapter);
        mPassRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        mPassRecyclerView.setLayoutManager(new OverlapLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mPassRecyclerView.setHasFixedSize(true);
        mPassRecyclerView.addItemDecoration(new PassStackItemDecoration(
                getResources().getDimensionPixelSize(R.dimen.stackOffset),
                getResources().getDimensionPixelSize(R.dimen.stackOffsetBottom)));

    }

    private List<Pass> generateMockUpPass() {
        List<Pass> passList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            passList.add(new Pass(i, "This is pass " + i));
        }

        return passList;
    }

    @Override
    public void onPassClicked(View itemView, Pass pass) {
        Toast.makeText(this, pass.getDescription(), Toast.LENGTH_SHORT).show();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, itemView, "passCardView");
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, Pair.create(itemView, "passCardView"), Pair.create((View) mPassRecyclerView, "passRecyclerView"));
            Intent intent = new Intent(MainActivity.this, PassDetailActivity.class);
            startActivity(intent, options.toBundle());
        } else {
            Intent intent = new Intent(this, PassDetailActivity.class);
            startActivity(intent);
        }


    }
}
